class CreateUserCalendars < ActiveRecord::Migration[5.2]
  def change
    create_table :user_calendars do |t|
      t.references :user, foreign_key: true
      t.json :preference

      t.timestamps
    end
  end
end
