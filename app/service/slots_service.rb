class SlotsService
    # SLOT_SIZE = 2 size 2 denotes 30 mins of 15 mins each

    def slots(user, month = 0, timezone = 'UTC', slot_size = 2 )
        @user_calendar = user.UserCalendar
        result = Array.new
        days_slots = slots_code(user, month = 0)
        days_slots.each do |date, per_day_slots|
            size = per_day_slots.size
            slot_index = 0
            while slot_index < size-(slot_size-1)
                i=1
                continuous_slots_present = true
                while i < slot_size
                    continuous_slots_present = false unless per_day_slots[slot_index + i] == per_day_slots[slot_index] + i
                    i+=1
                end
                starttime = (set_in_timezone(date.to_time)+(15*per_day_slots[slot_index]).minutes).in_time_zone(timezone)
                puts starttime
                puts continuous_slots_present
                result.push({ from: starttime, to: starttime + (15*slot_size).minutes }) if continuous_slots_present
                slot_index+=1
            end
        end
        result
    end

    def create_slot(user, name, email, starttime, endtime)
        client.post(user.token,starttime, endtime)
    end

    private
    
    def slots_code(user, month)
        
        starttime, endtime = duration(month)
        busy = busy_slots(user, @user_calendar.time_zone, @user_calendar.buffer, starttime, endtime)
        all_per_day = all_slots(user)
        week_days_available = @user_calendar.week_days_available
        
        result = {}
        startdate = starttime.to_date 
        endtime = endtime.to_date
        while startdate <= endtime do
            if week_days_available.include?(UserCalendar.week_days[startdate.strftime('%A')])
                result[startdate] = Array.new unless result[startdate]
                if busy[startdate]
                    result[startdate] += all_per_day - busy[startdate]
                else
                    result[startdate] += all_per_day
                end
            end
            startdate += 1.day
        end
        result
    end

    def busy_slots(user, timezone, buffer, starttime, endtime)
        result = {}
        client.get(user.token,starttime, endtime)[:calendars][:busy].each do |slot_range|
            starttime = slot_range[:start].to_time.in_time_zone(timezone).to_time - buffer.minutes
            endtime = slot_range[:end].to_time.in_time_zone(timezone).to_time + buffer.minutes
            while(starttime <= endtime) do
                result[starttime.to_date] = Array.new unless result[starttime.to_date]
                result[starttime.to_date].push(starttime.hour*4 + starttime.min/15)
                starttime += 15.minutes
            end
        end
        result
    end

    def all_slots(user)
        result = Array.new
        @user_calendar.available_day_range.each do |slot_range|

            temp = slot_range["start"].split(':')
            starttime = temp.first.to_i*4 + temp.second.to_i/15
            starttime += 1 if temp.second.to_i%15 > 0

            temp = slot_range["end"].split(':')
            endtime = temp.first.to_i*4 + temp.second.to_i/15
            endtime -= 1 if temp.second.to_i%15 > 0

            (starttime..(endtime-1)).each do |slot|
                result.push(slot)
            end
        end
        result.uniq.sort
    end

    def duration(month)
        if month == 0
            starttime = Time.now.in_time_zone(@user_calendar.time_zone)
            endtime = starttime.end_of_month
        end

        return [starttime, endtime]
    end

    def set_in_timezone(time)
        Time.use_zone(@user_calendar.time_zone) { time.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
    end

    def client  
        @client ||= Clients::GoogleCalendarClient.new
    end
end