class Clients::GoogleCalendarClient
    def get(token, startdate, enddate)
        mock_response
    end

    def post(token, startdate, enddate)
        # book slot in google calendar
        puts "book slot in google calendar from: #{startdate} to: #{enddate}"
        return {}
    end

    private

    def mock_response
        {
            "calendars": {
                "busy": [ {
                        "start":"2019-05-22T22:00:00Z",
                        "end":"2019-05-22T23:00:00Z" 
                    },
                    {
                        "start":"2019-05-23T22:00:00Z",
                        "end":"2019-05-23T23:00:00Z" 
                    },
                    {
                        "start":"2019-05-24T22:00:00Z",
                        "end":"2019-05-24T23:00:00Z" 
                    }    
                ]
            }

        }
    end

end