class SlotsController < ApplicationController

  # GET /user_calendars
  # GET /user_calendars.json
  def index
    @user_slots = slots_service.slots(user)
  end

  # POST /user_calendars
  # POST /user_calendars.json
  def create
    @user_calendar = UserCalendar.new(user_calendar_params)

    respond_to do |format|
      if @user_calendar.save
        format.html { redirect_to @user_calendar, notice: 'User calendar was successfully created.' }
        format.json { render :show, status: :created, location: @user_calendar }
      else
        format.html { render :new }
        format.json { render json: @user_calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def slots_service
    @slots_service ||= SlotsService.new
  end

  def user
    @user ||= User.find(params[:user_id])
  end
end
