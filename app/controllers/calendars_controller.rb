class CalendarsController < ApplicationController
  before_action :set_user_calendar, only: [:show, :edit, :update, :destroy]

  # GET /user_calendars
  # GET /user_calendars.json
  def index
    @user_calendars = UserCalendar.where(user_id: params[:user_id])
  end

  # GET /user_calendars/1
  # GET /user_calendars/1.json
  def show
  end

  # GET /user_calendars/new
  def new
    @user_calendar = UserCalendar.new
  end

  # GET /user_calendars/1/edit
  def edit
  end

  # POST /user_calendars
  # POST /user_calendars.json
  def create
    user_calendars_count = UserCalendar.where(user_id: params[:user_id]).count
    if(user_calendars_count > 0)
      redirect_to user_calendars_url, notice: 'User calendar already exists.'
      return
    end
    @user_calendar = UserCalendar.new(user_calendar_params)

    respond_to do |format|
      if @user_calendar.save
        format.html { redirect_to user_calendar_path(@user_calendar), notice: 'User calendar was successfully created.' }
        format.json { render :show, status: :created, location: @user_calendar }
      else
        format.html { render :new }
        format.json { render json: @user_calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_calendars/1
  # PATCH/PUT /user_calendars/1.json
  def update
    respond_to do |format|
      if @user_calendar.update(user_calendar_params)
        format.html { redirect_to @user_calendar, notice: 'User calendar was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_calendar }
      else
        format.html { render :edit }
        format.json { render json: @user_calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_calendars/1
  # DELETE /user_calendars/1.json
  def destroy
    @user_calendar.destroy
    respond_to do |format|
      format.html { redirect_to user_calendars_url, notice: 'User calendar was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_calendar
      @user_calendar = UserCalendar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_calendar_params
      user_calendar = params[:user_calendar]
      {

        user_id: params[:user_id], 
        preference: UserCalendar.create_preference(
          user_calendar[:buffer].to_i,
          JSON.parse(user_calendar[:per_day_slots]),
          JSON.parse(user_calendar[:week_days_available]),
          user_calendar[:time_zone])
      }
    end
end
