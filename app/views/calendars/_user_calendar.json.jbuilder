json.extract! user_calendar, :id, :user_id, :buffer, :per_day_slots, :week_days_available, :offset, :created_at, :updated_at
json.url user_calendar_url(user_calendar, format: :json)
