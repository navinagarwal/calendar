class UserCalendar < ApplicationRecord
  belongs_to :user
  MIN_SLOT_SIZE = 15

  # attr_accessor :preference

  enum week_day: [ :Sunday, :Monday, :Tuesday, :Wednesday, :Thursday, :Friday, :Saturday ]

  # Params:
  # buffer: integer in mins
  # per_day_slots: [
  #   {
  #     start: integer in mins
  #     end: integer in mins
  #   }
    
  # ]
  # week_days_available: array of integer, eg: [1,2,3,4,5] => Monday to Friday
  # time_zone: 
  def available_day_range
    preferences["availablility"]["per_day"]
  end

  def time_zone
    preferences["time_zone"]
  end

  def buffer
    preferences["buffer"]
  end

  def week_days_available
    preferences["availablility"]["per_week"]
  end

  def preferences
    JSON.parse(self.preference)
  end

  def self.create_preference (buffer, per_day_slots, week_days_available, time_zone)
    {
      buffer: buffer, 
      availablility: {
        per_day: per_day_slots,
        
        per_week: week_days_available
      },
      time_zone: time_zone

    }.to_json
  end
end
