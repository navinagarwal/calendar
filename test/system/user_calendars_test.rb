require "application_system_test_case"

class UserCalendarsTest < ApplicationSystemTestCase
  setup do
    @user_calendar = user_calendars(:one)
  end

  test "visiting the index" do
    visit user_calendars_url
    assert_selector "h1", text: "User Calendars"
  end

  test "creating a User calendar" do
    visit user_calendars_url
    click_on "New User Calendar"

    fill_in "Email", with: @user_calendar.email
    fill_in "End", with: @user_calendar.end
    fill_in "Name", with: @user_calendar.name
    fill_in "Offset", with: @user_calendar.offset
    fill_in "Start", with: @user_calendar.start
    fill_in "Stols", with: @user_calendar.stols
    fill_in "User", with: @user_calendar.user_id
    click_on "Create User calendar"

    assert_text "User calendar was successfully created"
    click_on "Back"
  end

  test "updating a User calendar" do
    visit user_calendars_url
    click_on "Edit", match: :first

    fill_in "Email", with: @user_calendar.email
    fill_in "End", with: @user_calendar.end
    fill_in "Name", with: @user_calendar.name
    fill_in "Offset", with: @user_calendar.offset
    fill_in "Start", with: @user_calendar.start
    fill_in "Stols", with: @user_calendar.stols
    fill_in "User", with: @user_calendar.user_id
    click_on "Update User calendar"

    assert_text "User calendar was successfully updated"
    click_on "Back"
  end

  test "destroying a User calendar" do
    visit user_calendars_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User calendar was successfully destroyed"
  end
end
