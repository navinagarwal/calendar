require 'test_helper'

class UserSlotsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_calendar = user_calendars(:one)
  end

  test "should get index" do
    get _user_calendars_url
    assert_response :success
  end

  test "should get new" do
    get new__user_calendar_url
    assert_response :success
  end

  test "should create user_calendar" do
    assert_difference('UserCalendar.count') do
      post _user_calendars_url, params: { user_calendar: { email: @user_calendar.email, end: @user_calendar.end, name: @user_calendar.name, offset: @user_calendar.offset, start: @user_calendar.start, stols: @user_calendar.stols, user_id: @user_calendar.user_id } }
    end

    assert_redirected_to user_calendar_url(UserCalendar.last)
  end

  test "should show user_calendar" do
    get _user_calendar_url(@user_calendar)
    assert_response :success
  end

  test "should get edit" do
    get edit__user_calendar_url(@user_calendar)
    assert_response :success
  end

  test "should update user_calendar" do
    patch _user_calendar_url(@user_calendar), params: { user_calendar: { email: @user_calendar.email, end: @user_calendar.end, name: @user_calendar.name, offset: @user_calendar.offset, start: @user_calendar.start, stols: @user_calendar.stols, user_id: @user_calendar.user_id } }
    assert_redirected_to user_calendar_url(@user_calendar)
  end

  test "should destroy user_calendar" do
    assert_difference('UserCalendar.count', -1) do
      delete _user_calendar_url(@user_calendar)
    end

    assert_redirected_to _user_calendars_url
  end
end
